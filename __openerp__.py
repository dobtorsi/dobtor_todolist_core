# -*- coding: utf-8 -*-
{
    'name' : "Dobtor_TodoList_Core",
    'version' : '1.0',
    'description' : 'Dobtor todo list core',
    'author' : "Steven, www.dobtor.com",
    'depends' : ['base','survey'],
    'data' : [
    ],
    'installable' : True,
    'application' : True,
}